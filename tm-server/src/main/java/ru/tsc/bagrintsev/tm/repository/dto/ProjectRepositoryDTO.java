package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

public class ProjectRepositoryDTO extends UserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(
            @NotNull final EntityManager entityManager
    ) {
        super(ProjectDTO.class, entityManager);
    }

}
