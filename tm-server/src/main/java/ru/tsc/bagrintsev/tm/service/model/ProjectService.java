package ru.tsc.bagrintsev.tm.service.model;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.model.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.model.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull Project add(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (project == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            project.setUser(user);
            repository.add(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @NotNull Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, ProjectNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable Project project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            if (status.equals(Status.IN_PROGRESS)) {
                project.setStatus(status);
                project.setDateStarted(new Date());
            } else if (status.equals(Status.COMPLETED)) {
                project.setStatus(status);
                project.setDateFinished(new Date());
            } else if (status.equals(Status.NOT_STARTED)) {
                project.setStatus(status);
                project.setDateStarted(null);
                project.setDateFinished(null);
            }
            update(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            repository.clearAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, ModelNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @Nullable final List<Project> list = repository.findAll();
            return (list == null) ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<Project> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @Nullable final List<Project> list = repository.findAllByUserId(userId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @NotNull final String order = getQueryOrder(sort);
            @Nullable final List<Project> list = repository.findAllSort(userId, order);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @Nullable Project project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable Project project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            repository.removeById(userId, id);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @NotNull Collection<Project> set(@NotNull final Collection<Project> projects) {
        if (projects.isEmpty()) return projects;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            repository.addAll(projects);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override
    public long totalCount() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            return repository.totalCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            return repository.totalCountByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final Project project) throws ModelNotFoundException {
        if (project == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            repository.update(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable Project project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            repository.updateById(userId, id, name, description);
            transaction.commit();
            project = repository.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
