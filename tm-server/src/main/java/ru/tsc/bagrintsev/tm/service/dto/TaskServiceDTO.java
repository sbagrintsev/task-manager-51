package ru.tsc.bagrintsev.tm.service.dto;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.repository.dto.TaskRepositoryDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    public TaskServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull TaskDTO add(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) throws ModelNotFoundException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (task == null) throw new ModelNotFoundException();
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.add(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @NotNull TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable TaskDTO task;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            if (status.equals(Status.IN_PROGRESS)) {
                task.setStatus(status);
                task.setDateStarted(new Date());
            } else if (status.equals(Status.COMPLETED)) {
                task.setStatus(status);
                task.setDateFinished(new Date());
            } else if (status.equals(Status.NOT_STARTED)) {
                task.setStatus(status);
                task.setDateStarted(null);
                task.setDateFinished(null);
            }
            repository.update(task);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.clearAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @Override
    public @NotNull TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            @Nullable final List<TaskDTO> result = repository.findAll();
            return (result == null) ? Collections.emptyList() : result;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<TaskDTO> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            List<TaskDTO> list = repository.findAllByUserId(userId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            @NotNull final String order = getQueryOrder(sort);
            List<TaskDTO> list = repository.findAllSort(userId, order);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            List<TaskDTO> list = repository.findAllByProjectId(userId, projectId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            @Nullable TaskDTO task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull TaskDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable TaskDTO task;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            repository.removeById(userId, id);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public @NotNull Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> tasks) {
        if (tasks.isEmpty()) return tasks;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.addAll(tasks);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasks;
    }

    @Override
    public @NotNull TaskDTO setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws TaskNotFoundException {
        @Nullable TaskDTO task;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.setProjectId(userId, taskId, projectId);
            transaction.commit();
            task = repository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public long totalCount() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            return repository.totalCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            return repository.totalCountByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable TaskDTO task;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final ITaskRepositoryDTO repository = new TaskRepositoryDTO(entityManager);
            repository.updateById(userId, id, name, description);
            transaction.commit();
            task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
