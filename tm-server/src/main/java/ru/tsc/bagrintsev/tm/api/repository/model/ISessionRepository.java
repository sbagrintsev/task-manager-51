package ru.tsc.bagrintsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Override
    void add(@NotNull final Session record);

    @Override
    void addAll(@NotNull final Collection<Session> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<Session> findAll();

    @Override
    @Nullable
    List<Session> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    Session findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final Session session);

}
