package ru.tsc.bagrintsev.tm.service.dto;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;
import ru.tsc.bagrintsev.tm.repository.dto.ProjectRepositoryDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO> implements IProjectServiceDTO {

    public ProjectServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull ProjectDTO add(
            @Nullable final String userId,
            @Nullable final ProjectDTO project
    ) throws ModelNotFoundException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (project == null) throw new ModelNotFoundException();
        project.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            repository.add(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @NotNull ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws IncorrectStatusException, IdIsEmptyException, ProjectNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (status == null) throw new IncorrectStatusException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            if (status.equals(Status.IN_PROGRESS)) {
                project.setStatus(status);
                project.setDateStarted(new Date());
            } else if (status.equals(Status.COMPLETED)) {
                project.setStatus(status);
                project.setDateFinished(new Date());
            } else if (status.equals(Status.NOT_STARTED)) {
                project.setStatus(status);
                project.setDateStarted(null);
                project.setDateFinished(null);
            }
            repository.update(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void clear(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            repository.clearAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    public @NotNull ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            @Nullable final List<ProjectDTO> list = repository.findAll();
            return (list == null) ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            @Nullable final List<ProjectDTO> list = repository.findAllByUserId(userId);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (sort == null) {
            return findAll(userId);
        }
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            @NotNull final String order = getQueryOrder(sort);
            @Nullable final List<ProjectDTO> list = repository.findAllSort(userId, order);
            return list == null ? Collections.emptyList() : list;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            @Nullable ProjectDTO project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            repository.removeById(userId, id);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @NotNull Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return projects;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            repository.addAll(projects);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override
    public long totalCount() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            return repository.totalCount();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long totalCount(@Nullable final String userId) throws IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
        try {
            return repository.totalCountByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            @NotNull final IProjectRepositoryDTO repository = new ProjectRepositoryDTO(entityManager);
            repository.updateById(userId, id, name, description);
            transaction.commit();
            project = repository.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
