package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.apache.ibatis.annotations.Mapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

@Mapper
public interface IUserRepositoryDTO extends IAbstractRepositoryDTO<UserDTO> {

    @Override
    void add(@NotNull final UserDTO model);

    @Override
    void addAll(@NotNull final Collection<UserDTO> records);

    @Override
    void clearAll();

    @Override
    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByEmail(@NotNull final String email);

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

    @Override
    @Nullable
    UserDTO findOneById(@NotNull final String id);

    boolean isEmailExists(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    void removeByLogin(@NotNull final String login);

    void setParameter(@NotNull final UserDTO user);

    void setRole(
            @NotNull final String login,
            @NotNull final Role role
    );

    void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    );

    @Override
    long totalCount();

}
