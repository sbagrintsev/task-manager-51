package ru.tsc.bagrintsev.tm.exception.user;

public final class PasswordIsIncorrectException extends AbstractUserException {

    public PasswordIsIncorrectException() {
        super("Error! Wrong password...");
    }

}
