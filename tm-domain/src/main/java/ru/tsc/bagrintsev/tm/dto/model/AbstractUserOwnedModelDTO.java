package ru.tsc.bagrintsev.tm.dto.model;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}
