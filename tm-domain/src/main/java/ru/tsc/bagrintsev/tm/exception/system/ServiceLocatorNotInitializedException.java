package ru.tsc.bagrintsev.tm.exception.system;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class ServiceLocatorNotInitializedException extends AbstractException {

    public ServiceLocatorNotInitializedException() {
        super("Error! Service Locator not initialized...");
    }

}
